port module Main exposing (Model, Msg(..), init, main, update, view)

import Browser
import Browser.Navigation
import Html
import Html.Attributes
import Html.Events
import Html.Parser
import Html.Parser.Util
import Http
import Json.Decode as Decode
import Json.Decode.Pipeline as Pipeline
import Json.Encode as Encode
import Task
import Time
import Url



---- MODEL ----


type alias Model =
    { key : Browser.Navigation.Key -- Only used as a safeguard by utilities like Browser.Navigation.pushUrl
    , connectedState : ConnectedState
    , zone : Time.Zone
    , url : Url.Url
    , notifications : Notifications
    }


type ConnectedState
    = LoggedOut LoggedOutModel
    | LoggingIn
    | LoggedIn LoggedInModel


type alias LoggedOutModel =
    { server : String
    , username : String
    , email : String
    }


type Server
    = Server String


type alias LoggedInModel =
    { server : Server
    , entries : RemoteData Entry
    , feeds : RemoteData Feed
    , page : Page
    , refreshing : Bool
    , filter : Filter
    , progress : Float
    , selectedEntry : Maybe String
    , selectedFeed : Maybe Feed
    }


type RemoteData a
    = Requested
    | Received (List a)
    | Error Http.Error


type alias Entry =
    { id : Int
    , title : String
    , summary : String
    , content : String
    , flagged : Bool
    , seen : Bool
    , bookmark : Bool
    , image : String
    , link : String
    , updated : Time.Posix
    , sources : List Feed
    }


type alias Feed =
    { title : String
    , subtitle : String
    , link : String
    , active : Bool
    , failed : Maybe Time.Posix
    }


type OriginalFeed
    = OriginalFeed Feed


type Page
    = HomePage
    | NewFeedPage String
    | EditFeedPage OriginalFeed Feed


type Filter
    = All
    | Unseen
    | Bookmarked
    | Trending


type alias Notifications =
    List String


type alias Flags =
    { server : Maybe String }


init : Flags -> Url.Url -> Browser.Navigation.Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( model, cmd ) =
            case flags.server of
                Nothing ->
                    initLoggedOut

                Just server ->
                    initLoggedIn url <| Server server
    in
    ( { key = key
      , url = url
      , connectedState = model
      , zone = Time.utc
      , notifications = []
      }
    , Cmd.batch
        [ Task.perform AdjustTimeZone Time.here
        , cmd
        ]
    )


initLoggedOut : ( ConnectedState, Cmd Msg )
initLoggedOut =
    ( LoggedOut { server = "", username = "", email = "" }, Cmd.none )


initLoggedIn : Url.Url -> Server -> ( ConnectedState, Cmd Msg )
initLoggedIn url server =
    ( LoggedIn
        { server = server
        , entries = Requested
        , feeds = Requested
        , page = urlToPage url
        , refreshing = True
        , filter = Unseen
        , progress = 0
        , selectedEntry = Nothing
        , selectedFeed = Nothing
        }
    , Cmd.batch [ getEntries server, getFeeds server ]
        |> Cmd.map LoggedInMsg
    )


urlToPage : Url.Url -> Page
urlToPage { fragment } =
    case fragment of
        Just "new-feed" ->
            NewFeedPage ""

        _ ->
            HomePage



---- UPDATE ----


type Msg
    = AdjustTimeZone Time.Zone
    | LoggedOutMsg LoggedOutMsg
    | LoggedInMsg LoggedInMsg
    | UrlChanged Url.Url
    | LinkClicked Browser.UrlRequest
    | DiscardNotification Int


type alias LoginForm =
    { server : String }


type LoggedOutMsg
    = UpdateServer String
    | UpdateUsername String
    | UpdateEmail String
    | TokenAsked (Result Http.Error ())
    | Login


type LoggedInMsg
    = NewEntries (Result Http.Error (List Entry))
    | NewFeeds (Result Http.Error (List Feed))
    | AddingNewFeed String
    | AddNewFeed String
    | NewFeedAdded (Result Http.Error Feed)
    | EditingFeed OriginalFeed Feed
    | EditFeed OriginalFeed Feed
    | EditedFeed OriginalFeed (Result Http.Error Feed)
    | DeleteFeed Feed
    | DeletedFeed (Result Http.Error Feed)
    | Sync
    | Flag Entry
    | Bookmark Entry
    | MarkSeen Entry
    | UpdatedEntry Entry (Result Http.Error Entry)
    | UpdateFilter Filter
    | UpdateProgress Float
    | ProgressDone Bool
    | SelectedEntry String
    | ToggleFeed Feed
    | MarkAllSeen
    | AllMarkedAsSeen (Result Http.Error Int)
    | Logout


update : Msg -> Model -> ( Model, Cmd Msg )
update msg globalModel =
    let
        toLoggedOut : ( LoggedOutModel, Cmd LoggedOutMsg ) -> ( Model, Cmd Msg )
        toLoggedOut ( model, cmd ) =
            ( { globalModel | connectedState = LoggedOut model }
            , Cmd.map LoggedOutMsg cmd
            )

        toLoggedIn : ( LoggedInModel, Cmd LoggedInMsg ) -> ( Model, Cmd Msg )
        toLoggedIn ( model, cmd ) =
            ( { globalModel | connectedState = LoggedIn model }
            , Cmd.map LoggedInMsg cmd
            )

        withNotification : String -> ( Model, Cmd Msg ) -> ( Model, Cmd Msg )
        withNotification notif ( model, cmd ) =
            ( { model | notifications = model.notifications ++ [ notif ] }, cmd )
    in
    case ( msg, globalModel.connectedState ) of
        ---- GLOBAL MESSAGES ----
        ( LinkClicked urlRequest, _ ) ->
            case urlRequest of
                Browser.Internal url ->
                    ( globalModel, Browser.Navigation.pushUrl globalModel.key (Url.toString url) )

                Browser.External href ->
                    ( globalModel, Browser.Navigation.load href )

        ( UrlChanged url, LoggedOut _ ) ->
            ( { globalModel | url = url }, Cmd.none )

        ( UrlChanged url, LoggedIn model ) ->
            ( { globalModel
                | url = url
                , connectedState = LoggedIn { model | page = urlToPage url }
              }
            , Cmd.none
            )

        ( AdjustTimeZone zone, _ ) ->
            ( { globalModel | zone = zone }, Cmd.none )

        ---- LOGGED OUT MESSAGES ----
        ( LoggedOutMsg (UpdateServer server), LoggedOut model ) ->
            ( { model | server = server }, Cmd.none )
                |> toLoggedOut

        ( LoggedOutMsg (UpdateUsername username), LoggedOut model ) ->
            ( { model | username = username }, Cmd.none )
                |> toLoggedOut

        ( LoggedOutMsg (UpdateEmail email), LoggedOut model ) ->
            ( { model | email = email }, Cmd.none )
                |> toLoggedOut

        ( LoggedOutMsg Login, LoggedOut model ) ->
            ( model
            , Cmd.batch
                [ askToken model
                , saveSession model.server
                ]
            )
                |> toLoggedOut

        ( LoggedOutMsg (TokenAsked (Ok ())), LoggedOut model ) ->
            ( { globalModel | connectedState = LoggingIn }
            , Cmd.none
            )

        ( LoggedOutMsg (TokenAsked (Err error)), LoggedOut model ) ->
            ( globalModel
            , Cmd.none
            )
                |> withNotification ("Error while logging in: " ++ errorToString error)

        ---- LOGGED IN MESSAGES ----
        ( LoggedInMsg (NewEntries (Err error)), LoggedIn model ) ->
            ( { model | entries = Error error, refreshing = False }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (NewEntries (Ok entries)), LoggedIn model ) ->
            ( { model | entries = Received entries, refreshing = False }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (NewFeeds (Err error)), LoggedIn model ) ->
            ( { model | feeds = Error error }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (NewFeeds (Ok feeds)), LoggedIn model ) ->
            ( { model | feeds = Received feeds }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (AddingNewFeed feedUrl), LoggedIn model ) ->
            ( { model | page = NewFeedPage feedUrl }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (AddNewFeed feedUrl), LoggedIn model ) ->
            let
                jsonBody =
                    [ ( "link", Encode.string feedUrl ) ]
                        |> Encode.object
                        |> Http.jsonBody
            in
            ( model
            , riskyPostRequest (urlFromServer model.server ++ "/feed") jsonBody feedDecoder
                |> Http.send NewFeedAdded
            )
                |> toLoggedIn

        ( LoggedInMsg (NewFeedAdded (Err error)), LoggedIn model ) ->
            ( model, Cmd.none )
                |> toLoggedIn
                |> withNotification "Error while adding new feed"

        ( LoggedInMsg (NewFeedAdded (Ok feed)), LoggedIn model ) ->
            let
                updatedFeeds =
                    case model.feeds of
                        Received feeds ->
                            if List.member feed feeds then
                                model.feeds

                            else
                                Received (feeds ++ [ feed ] |> List.sortBy .title)

                        _ ->
                            Received [ feed ]
            in
            ( { model | feeds = updatedFeeds, refreshing = True }, Cmd.batch [ Browser.Navigation.pushUrl globalModel.key "#", getEntries model.server ] )
                |> toLoggedIn

        ( LoggedInMsg (EditingFeed originalFeed feed), LoggedIn model ) ->
            ( { model | page = EditFeedPage originalFeed feed }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (EditFeed (OriginalFeed originalFeed) feed), LoggedIn model ) ->
            let
                jsonBody =
                    [ ( "link", Encode.string feed.link )
                    , ( "title", Encode.string feed.title )
                    , ( "subtitle", Encode.string feed.subtitle )
                    , ( "active", Encode.bool feed.active )
                    ]
                        |> Encode.object
                        |> Http.jsonBody
            in
            ( model
            , riskyPostRequest (urlFromServer model.server ++ "/feed/" ++ originalFeed.link) jsonBody feedDecoder
                |> Http.send (EditedFeed (OriginalFeed originalFeed))
            )
                |> toLoggedIn

        ( LoggedInMsg (EditedFeed originalFeed (Err error)), LoggedIn model ) ->
            ( model, Cmd.none )
                |> toLoggedIn
                |> withNotification "Error while editing feed"

        ( LoggedInMsg (EditedFeed (OriginalFeed originalFeed) (Ok feed)), LoggedIn model ) ->
            let
                updatedFeeds =
                    Received <|
                        case model.feeds of
                            Received feeds ->
                                listReplace originalFeed feed feeds

                            _ ->
                                [ feed ]

                updatedEntries =
                    case model.entries of
                        Received entries ->
                            -- Change all the entries' sources from the original feed to the newly updated one
                            entries
                                |> List.map
                                    (\entry ->
                                        { entry
                                            | sources =
                                                listReplace originalFeed feed entry.sources
                                        }
                                    )
                                |> Received

                        _ ->
                            model.entries
            in
            ( { model | feeds = updatedFeeds, entries = updatedEntries }, Browser.Navigation.pushUrl globalModel.key "#" )
                |> toLoggedIn

        ( LoggedInMsg (DeleteFeed feed), LoggedIn model ) ->
            let
                request : Http.Request Feed
                request =
                    { method = "DELETE"
                    , headers = []
                    , url = urlFromServer model.server ++ "/feed/" ++ feed.link
                    , body = Http.emptyBody
                    , expect = Http.expectJson feedDecoder
                    , timeout = Nothing
                    , withCredentials = False
                    }
                        |> Http.request
            in
            ( model
            , Http.send DeletedFeed request
            )
                |> toLoggedIn

        ( LoggedInMsg (DeletedFeed (Err error)), LoggedIn model ) ->
            ( model, Cmd.none )
                |> toLoggedIn
                |> withNotification "Error while deleting feed"

        ( LoggedInMsg (DeletedFeed (Ok feed)), LoggedIn model ) ->
            let
                updatedFeeds =
                    Received <|
                        case model.feeds of
                            Received feeds ->
                                feeds
                                    |> List.filter (\f -> f.link /= feed.link)

                            _ ->
                                []
            in
            ( { model | feeds = updatedFeeds }, Browser.Navigation.pushUrl globalModel.key "#" )
                |> toLoggedIn

        ( LoggedInMsg Sync, LoggedIn model ) ->
            ( { model | refreshing = True }, sync <| urlFromServer model.server )
                |> toLoggedIn

        ( LoggedInMsg (Flag entry), LoggedIn model ) ->
            let
                jsonBody =
                    [ ( "flagged", Encode.bool (not entry.flagged) ) ]
                        |> Encode.object
                        |> Http.jsonBody

                link =
                    entry.link
                        |> Url.percentEncode
            in
            ( { model | refreshing = True }
            , riskyPostRequest (urlFromServer model.server ++ "/entry/" ++ link) jsonBody entryDecoder
                |> Http.send (UpdatedEntry entry)
            )
                |> toLoggedIn

        ( LoggedInMsg (Bookmark entry), LoggedIn model ) ->
            let
                jsonBody =
                    [ ( "bookmark", Encode.bool (not entry.bookmark) ) ]
                        |> Encode.object
                        |> Http.jsonBody

                link =
                    entry.link
                        |> Url.percentEncode
            in
            ( { model | refreshing = True }
            , riskyPostRequest (urlFromServer model.server ++ "/entry/" ++ link) jsonBody entryDecoder
                |> Http.send (UpdatedEntry entry)
            )
                |> toLoggedIn

        ( LoggedInMsg (MarkSeen entry), LoggedIn model ) ->
            let
                jsonBody =
                    [ ( "seen", Encode.bool (not entry.seen) ) ]
                        |> Encode.object
                        |> Http.jsonBody

                link =
                    entry.link
                        |> Url.percentEncode
            in
            ( { model | refreshing = True }
            , riskyPostRequest (urlFromServer model.server ++ "/entry/" ++ link) jsonBody entryDecoder
                |> Http.send (UpdatedEntry entry)
            )
                |> toLoggedIn

        ( LoggedInMsg (UpdatedEntry originalEntry (Err error)), LoggedIn model ) ->
            ( { model | refreshing = False }, Cmd.none )
                |> toLoggedIn
                |> withNotification "Error while updating entry"

        ( LoggedInMsg (UpdatedEntry originalEntry (Ok entry)), LoggedIn model ) ->
            let
                updatedEntries =
                    Received <|
                        case model.entries of
                            Received entries ->
                                listReplace originalEntry entry entries

                            _ ->
                                [ entry ]
            in
            ( { model | entries = updatedEntries, refreshing = False }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (UpdateFilter filter), LoggedIn model ) ->
            ( { model | filter = filter }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (UpdateProgress amount), LoggedIn model ) ->
            ( { model | progress = amount }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (ProgressDone _), LoggedIn model ) ->
            ( { model | progress = 0 }, Cmd.batch [ getEntries model.server, getFeeds model.server ] )
                |> toLoggedIn

        ( LoggedInMsg (SelectedEntry entryLink), LoggedIn model ) ->
            ( { model | selectedEntry = Just entryLink }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg (ToggleFeed feed), LoggedIn model ) ->
            case model.selectedFeed of
                Nothing ->
                    ( { model | selectedFeed = Just feed }, Cmd.none )
                        |> toLoggedIn

                Just selectedFeed ->
                    if selectedFeed == feed then
                        ( { model | selectedFeed = Nothing }, Cmd.none )
                            |> toLoggedIn

                    else
                        ( { model | selectedFeed = Just feed }, Cmd.none )
                            |> toLoggedIn

        ( LoggedInMsg MarkAllSeen, LoggedIn model ) ->
            let
                decoder =
                    Decode.field "updated" Decode.int
            in
            ( model
            , riskyPostRequest (urlFromServer model.server ++ "/mark-all-seen") Http.emptyBody decoder
                |> Http.send AllMarkedAsSeen
            )
                |> toLoggedIn

        ( LoggedInMsg (AllMarkedAsSeen (Err error)), LoggedIn model ) ->
            ( { model | refreshing = False }, Cmd.none )
                |> toLoggedIn
                |> withNotification "Error while marking all entries as seen"

        ( LoggedInMsg (AllMarkedAsSeen (Ok _)), LoggedIn model ) ->
            let
                updatedEntries =
                    Received <|
                        case model.entries of
                            Received entries ->
                                entries
                                    |> List.map
                                        (\entry ->
                                            { entry | seen = True }
                                        )

                            _ ->
                                []
            in
            ( { model | entries = updatedEntries, refreshing = False }, Cmd.none )
                |> toLoggedIn

        ( LoggedInMsg Logout, LoggedIn _ ) ->
            let
                ( model, cmd ) =
                    initLoggedOut
            in
            ( { globalModel | connectedState = model }
            , Cmd.batch [ cmd, removeSession () ]
            )

        ( DiscardNotification index, _ ) ->
            ( { globalModel | notifications = listRemoveAtIndex index globalModel.notifications }, Cmd.none )

        ( _, _ ) ->
            ( globalModel, Cmd.none )
                |> withNotification "oops, bad routing"



---- VIEW ----


view : Model -> Browser.Document Msg
view globalModel =
    let
        withNotifications : List (Html.Html Msg) -> List (Html.Html Msg)
        withNotifications body =
            viewNotifications globalModel.notifications :: body
    in
    case globalModel.connectedState of
        LoggedOut model ->
            let
                { title, body } =
                    viewLoginForm model
            in
            { title = title
            , body =
                body
                    |> List.map (Html.map LoggedOutMsg)
                    |> withNotifications
            }

        LoggingIn ->
            let
                { title, body } =
                    viewLoggingIn
            in
            { title = title
            , body =
                body
                    |> List.map (Html.map LoggedOutMsg)
                    |> withNotifications
            }

        LoggedIn model ->
            let
                { title, body } =
                    viewLoggedIn model globalModel.zone
            in
            { title = title
            , body =
                body
                    |> List.map (Html.map LoggedInMsg)
                    |> withNotifications
            }


viewLoginForm : LoggedOutModel -> Browser.Document LoggedOutMsg
viewLoginForm model =
    { title = "Login | reRSS client"
    , body =
        [ Html.section [ Html.Attributes.class "main" ]
            [ Html.div []
                [ Html.form
                    [ Html.Events.onSubmit Login
                    ]
                    [ Html.fieldset []
                        [ Html.legend [] [ Html.text "Select your server" ]
                        , Html.div [ Html.Attributes.class "input-single" ]
                            [ Html.label [ Html.Attributes.for "server" ] [ Html.text "Server" ]
                            , Html.input
                                [ Html.Attributes.type_ "text"
                                , Html.Attributes.class "input-big"
                                , Html.Attributes.placeholder "Server URL"
                                , Html.Attributes.value model.server
                                , Html.Attributes.id "server"
                                , Html.Events.onInput UpdateServer
                                ]
                                []
                            , Html.label [ Html.Attributes.for "username" ] [ Html.text "Username" ]
                            , Html.input
                                [ Html.Attributes.type_ "text"
                                , Html.Attributes.class "input-big"
                                , Html.Attributes.placeholder "your username, eg: julia123"
                                , Html.Attributes.value model.username
                                , Html.Attributes.id "username"
                                , Html.Events.onInput UpdateUsername
                                ]
                                []
                            , Html.label [ Html.Attributes.for "email" ] [ Html.text "Email" ]
                            , Html.input
                                [ Html.Attributes.type_ "text"
                                , Html.Attributes.class "input-big"
                                , Html.Attributes.placeholder "your email, eg: julia@example.com"
                                , Html.Attributes.value model.email
                                , Html.Attributes.id "email"
                                , Html.Events.onInput UpdateEmail
                                ]
                                []
                            ]
                        , Html.div [ Html.Attributes.class "input-single" ]
                            [ Html.input
                                [ Html.Attributes.type_ "submit"
                                , Html.Attributes.class "button"
                                , Html.Attributes.value "Get my feeds from this server"
                                ]
                                []
                            ]
                        ]
                    ]
                ]
            ]
        ]
    }


viewLoggingIn : Browser.Document LoggedOutMsg
viewLoggingIn =
    { title = "Login in | reRSS client"
    , body =
        [ Html.section [ Html.Attributes.class "main" ]
            [ Html.div [] [ Html.text "An email has been sent with a login link" ]
            ]
        ]
    }


viewLoggedIn : LoggedInModel -> Time.Zone -> Browser.Document LoggedInMsg
viewLoggedIn model zone =
    let
        feedEntries =
            case model.page of
                HomePage ->
                    case model.entries of
                        Requested ->
                            Html.text "Entries have been requested, please hold tight!"

                        Received entries ->
                            viewEntries entries model.filter model.selectedFeed model.selectedEntry zone

                        Error error ->
                            Html.text <| "An error occured while requesting the entries: " ++ errorToString error

                NewFeedPage newFeedUrl ->
                    viewNewFeed newFeedUrl

                EditFeedPage originalFeed feed ->
                    viewEditFeed zone originalFeed feed

        feedList =
            case model.feeds of
                Requested ->
                    Html.text "loading feeds"

                Received feeds ->
                    viewFeeds feeds model.selectedFeed

                Error error ->
                    Html.text <| "An error occured while requesting the feeds: " ++ errorToString error

        selectedEntry =
            case model.selectedEntry of
                Just entryLink ->
                    case model.entries of
                        Received entries ->
                            entries
                                |> List.filter (\entry -> entry.link == entryLink)
                                |> List.head

                        _ ->
                            Nothing

                Nothing ->
                    Nothing
    in
    { title = "Listing | reRSS client"
    , body =
        [ Html.section [ Html.Attributes.class "main" ]
            [ viewHeader model.server model.refreshing model.progress
            , feedList
            , feedEntries
            , viewEntry selectedEntry
            ]
        ]
    }


viewNotifications : Notifications -> Html.Html Msg
viewNotifications notifications =
    let
        viewNotification index notif =
            Html.div [ Html.Attributes.class "alert alert-secondary" ]
                [ Html.text notif
                , Html.button
                    [ Html.Events.onClick <| DiscardNotification index ]
                    [ Html.i [ Html.Attributes.class "fa fa-times" ] []
                    ]
                ]
    in
    Html.section [ Html.Attributes.class "notifications" ]
        (notifications
            |> List.indexedMap viewNotification
        )


viewHeader : Server -> Bool -> Float -> Html.Html LoggedInMsg
viewHeader server refreshing currentProgress =
    let
        serverURL =
            urlFromServer server
    in
    Html.header [ Html.Attributes.class "header" ]
        [ Html.progress
            [ Html.Attributes.style "display"
                (if currentProgress /= 0 then
                    "block"

                 else
                    "none"
                )
            , Html.Attributes.id "progress"
            , Html.Attributes.value <| String.fromFloat currentProgress
            ]
            []
        , Html.section [ Html.Attributes.class "server text-right" ]
            [ Html.text <| "Connected to " ++ serverURL ++ " "
            , Html.a
                [ Html.Attributes.href "#"
                , Html.Attributes.class "button button-link"
                , Html.Events.onClick Logout
                ]
                [ Html.i [ Html.Attributes.class "fa fa-sign-out-alt" ] [] ]
            ]
        , Html.section []
            [ Html.a
                [ Html.Attributes.href "#feeds"
                , Html.Attributes.class "button button-link"
                ]
                [ Html.text "Feeds" ]
            , Html.a
                [ Html.Attributes.href <| serverURL ++ "/myfeed/atom"
                , Html.Attributes.class "button button-link"
                ]
                [ Html.i [ Html.Attributes.class "fa fa-rss" ] []
                , Html.text " My reRSS"
                ]
            ]
        , Html.section [ Html.Attributes.class "text-center" ]
            [ Html.a [ Html.Attributes.href serverURL ] [ Html.text "uRSS" ]
            ]
        , Html.section [ Html.Attributes.class "text-right" ]
            [ Html.a
                [ Html.Attributes.href "#"
                , Html.Attributes.title "Sync the RSS feeds"
                , Html.Attributes.class "button"
                , Html.Events.onClick Sync
                ]
                [ Html.i
                    [ Html.Attributes.class <|
                        "fa fa-sync-alt"
                            ++ (if refreshing then
                                    " fa-spin"

                                else
                                    ""
                               )
                    ]
                    []
                ]
            , Html.text " "
            , Html.a
                [ Html.Attributes.href "#new-feed"
                , Html.Attributes.class "button"
                ]
                [ Html.i [ Html.Attributes.class "fa fa-plus" ] []
                , Html.text " New feed"
                ]
            ]
        ]


viewFeeds : List Feed -> Maybe Feed -> Html.Html LoggedInMsg
viewFeeds feeds selectedFeed =
    let
        feedStyle feed =
            case selectedFeed of
                Nothing ->
                    Html.Attributes.style "" ""

                Just selected ->
                    if selected == feed then
                        Html.Attributes.style "font-weight" "bold"

                    else
                        Html.Attributes.style "" ""
    in
    Html.aside [ Html.Attributes.class "feed-list" ]
        [ Html.ul []
            (feeds
                |> List.map
                    (\feed ->
                        Html.li
                            [ Html.Attributes.classList
                                [ ( "inactive", not feed.active )
                                , ( "sync-failed"
                                  , feed.failed
                                        |> Maybe.map (\_ -> True)
                                        |> Maybe.withDefault False
                                  )
                                ]
                            ]
                            [ Html.a
                                [ Html.Attributes.class "btn"
                                , Html.Attributes.href "#"
                                , Html.Events.onClick (EditingFeed (OriginalFeed feed) feed)
                                ]
                                [ Html.i [ Html.Attributes.class "fa fa-pencil-alt" ] [] ]
                            , Html.a
                                [ Html.Attributes.href "#"
                                , Html.Events.onClick <| ToggleFeed feed
                                , feedStyle feed
                                ]
                                [ Html.text (" " ++ feed.title) ]
                            ]
                    )
            )
        ]


viewEntries : List Entry -> Filter -> Maybe Feed -> Maybe String -> Time.Zone -> Html.Html LoggedInMsg
viewEntries entries currentFilter selectedFeed selectedEntry zone =
    let
        filteredEntries =
            case currentFilter of
                All ->
                    entries

                Unseen ->
                    List.filter (\e -> not e.seen) entries

                Bookmarked ->
                    List.filter (\e -> e.bookmark) entries

                Trending ->
                    entries

        selectedEntries =
            case selectedFeed of
                Nothing ->
                    filteredEntries

                Just feed ->
                    filteredEntries
                        |> List.filter
                            (\entry ->
                                List.member feed entry.sources
                            )
    in
    Html.div [ Html.Attributes.class "feed-entries" ]
        [ viewTabs currentFilter
        , viewActions currentFilter
        , Html.div [ Html.Attributes.class "cards" ]
            (List.map (viewEntryItem zone selectedEntry) selectedEntries)
        ]


viewTabs : Filter -> Html.Html LoggedInMsg
viewTabs currentFilter =
    let
        filterItem label filter =
            Html.a
                [ Html.Attributes.class <|
                    if currentFilter == filter then
                        "active"

                    else
                        ""
                , Html.Attributes.href "#"
                , Html.Events.onClick <| UpdateFilter filter
                ]
                [ Html.text label ]
    in
    Html.div [ Html.Attributes.class "tabs" ]
        [ Html.nav [ Html.Attributes.class "tabs-nav" ]
            [ filterItem "All" All
            , filterItem "Unseen" Unseen
            , filterItem "Bookmarked" Bookmarked
            , filterItem "Trending" Trending
            ]
        ]


viewActions : Filter -> Html.Html LoggedInMsg
viewActions currentFilter =
    let
        actions =
            case currentFilter of
                Unseen ->
                    [ Html.button [ Html.Events.onClick MarkAllSeen ] [ Html.text "Mark all as seen" ] ]

                _ ->
                    []
    in
    Html.div [ Html.Attributes.class "actions" ]
        [ Html.ul []
            [ Html.li []
                actions
            ]
        ]


viewEntryItem : Time.Zone -> Maybe String -> Entry -> Html.Html LoggedInMsg
viewEntryItem zone selectedEntry entry =
    let
        selectedClass =
            selectedEntry
                |> Maybe.withDefault ""
                |> (\selected ->
                        if selected == entry.link then
                            " selected"

                        else
                            ""
                   )

        titleNode =
            Html.h4 []
                [ Html.text entry.title ]

        button bool ( titleTrue, iconTrue ) ( titleFalse, iconFalse ) msg =
            Html.button
                [ Html.Attributes.title
                    (if bool then
                        titleTrue

                     else
                        titleFalse
                    )
                , Html.Events.onClick <| msg entry
                ]
                (if bool then
                    [ Html.i [ Html.Attributes.class iconTrue ]
                        []
                    ]

                 else
                    [ Html.i [ Html.Attributes.class iconFalse ]
                        []
                    ]
                )
    in
    Html.div [ Html.Attributes.class <| "card" ++ selectedClass ]
        [ Html.h5 []
            [ Html.span [ Html.Attributes.class "label" ]
                (entry.sources
                    |> List.map (\e -> e.title)
                    |> List.intersperse " ∙ "
                    |> List.map Html.text
                )
            ]
        , Html.h6 []
            [ Html.text <| displayTime entry.updated zone
            ]
        , Html.a [ Html.Attributes.href "#", Html.Events.onClick <| SelectedEntry entry.link ]
            [ titleNode ]
        , Html.footer []
            [ button
                entry.flagged
                ( "Remove from your ReRSS feed", "fa fa-plus-square" )
                ( "Add to your ReRSS feed", "far fa-plus-square" )
                Flag
            , button
                entry.bookmark
                ( "Remove from your bookmarks", "fa fa-bookmark" )
                ( "Bookmark: Read it later", "far fa-bookmark" )
                Bookmark
            , button
                entry.seen
                ( "Mark as unseen", "far fa-envelope-open" )
                ( "Mark as seen", "far fa-envelope" )
                MarkSeen
            , Html.a [ Html.Attributes.class "button", Html.Attributes.href entry.link, Html.Attributes.title "Open" ]
                [ Html.i [ Html.Attributes.class "fa fa-link" ]
                    []
                ]
            ]
        ]


viewNewFeed : String -> Html.Html LoggedInMsg
viewNewFeed newFeedUrl =
    Html.div []
        [ Html.form [ Html.Events.onSubmit (AddNewFeed newFeedUrl) ]
            [ Html.fieldset []
                [ Html.legend []
                    [ Html.text "Add a new feed" ]
                , Html.div [ Html.Attributes.class "input-single" ]
                    [ Html.label [ Html.Attributes.for "link" ]
                        [ Html.text "URL" ]
                    , Html.input
                        [ Html.Attributes.class "input-big"
                        , Html.Attributes.id "link"
                        , Html.Attributes.placeholder "Feed URL"
                        , Html.Attributes.type_ "text"
                        , Html.Events.onInput AddingNewFeed
                        , Html.Attributes.value newFeedUrl
                        ]
                        []
                    ]
                , Html.div [ Html.Attributes.class "input-single" ]
                    [ Html.input [ Html.Attributes.class "button", Html.Attributes.type_ "submit", Html.Attributes.value "Create new feed" ]
                        []
                    , Html.a [ Html.Attributes.class "button button-link", Html.Attributes.href "/", Html.Attributes.type_ "reset" ]
                        [ Html.text "Cancel" ]
                    ]
                ]
            ]
        ]


viewEditFeed : Time.Zone -> OriginalFeed -> Feed -> Html.Html LoggedInMsg
viewEditFeed zone originalFeed feed =
    Html.form [ Html.Events.onSubmit (EditFeed originalFeed feed) ]
        [ Html.fieldset []
            [ Html.legend []
                [ Html.text "Edit feed" ]
            , Html.div [ Html.Attributes.class "input-single" ]
                [ Html.label [ Html.Attributes.for "title" ]
                    [ Html.text "Title" ]
                , Html.input
                    [ Html.Attributes.class "input-big"
                    , Html.Attributes.id "title"
                    , Html.Attributes.placeholder "Feed title"
                    , Html.Attributes.type_ "text"
                    , Html.Attributes.value feed.title
                    , Html.Events.onInput (\newTitle -> EditingFeed originalFeed { feed | title = newTitle })
                    ]
                    []
                ]
            , Html.div [ Html.Attributes.class "input-single" ]
                [ Html.label [ Html.Attributes.for "subtitle" ]
                    [ Html.text "Subtitle" ]
                , Html.input
                    [ Html.Attributes.class "input-big"
                    , Html.Attributes.id "subtitle"
                    , Html.Attributes.placeholder "Feed URL"
                    , Html.Attributes.type_ "text"
                    , Html.Attributes.value feed.subtitle
                    , Html.Events.onInput (\newSubtitle -> EditingFeed originalFeed { feed | subtitle = newSubtitle })
                    ]
                    []
                ]
            , Html.div [ Html.Attributes.class "input-single" ]
                [ Html.label [ Html.Attributes.for "link" ]
                    [ Html.text "URL" ]
                , Html.input
                    [ Html.Attributes.class "input-big"
                    , Html.Attributes.id "link"
                    , Html.Attributes.placeholder "Feed URL"
                    , Html.Attributes.type_ "text"
                    , Html.Attributes.value feed.link
                    , Html.Events.onInput (\newLink -> EditingFeed originalFeed { feed | link = newLink })
                    ]
                    []
                ]
            , Html.div [ Html.Attributes.class "input-single" ]
                [ Html.span [ Html.Attributes.class "switch" ]
                    [ Html.input
                        [ Html.Attributes.checked feed.active
                        , Html.Attributes.id "active"
                        , Html.Attributes.class "switch"
                        , Html.Attributes.name "active"
                        , Html.Attributes.type_ "checkbox"
                        , Html.Events.onCheck (\newActive -> EditingFeed originalFeed { feed | active = newActive })
                        ]
                        []
                    , Html.label [ Html.Attributes.for "active" ]
                        [ Html.text "Active" ]
                    ]
                ]
            , Html.div [ Html.Attributes.class "input-single" ]
                [ Html.span [ Html.Attributes.class "sync-failed" ]
                    [ feed.failed
                        |> Maybe.map (\failed -> "Last failed syncing on: " ++ displayTime failed zone)
                        |> Maybe.withDefault ""
                        |> Html.text
                    ]
                ]
            , Html.div [ Html.Attributes.class "input-single" ]
                [ Html.input
                    [ Html.Attributes.class "button"
                    , Html.Attributes.type_ "submit"
                    , Html.Attributes.value "Save"
                    ]
                    []
                , Html.a
                    [ Html.Attributes.class "button button-link"
                    , Html.Attributes.href "/"
                    , Html.Attributes.type_ "reset"
                    ]
                    [ Html.text "Cancel" ]
                , Html.button
                    [ Html.Attributes.class "button-danger align-right"
                    , Html.Attributes.type_ "button"
                    , Html.Events.onClick <| DeleteFeed feed
                    ]
                    [ Html.text "Delete" ]
                ]
            ]
        ]


viewEntry : Maybe Entry -> Html.Html LoggedInMsg
viewEntry maybeEntry =
    case maybeEntry of
        Nothing ->
            Html.text ""

        Just entry ->
            let
                button bool ( titleTrue, labelTrue, iconTrue ) ( titleFalse, labelFalse, iconFalse ) msg =
                    Html.button
                        [ Html.Attributes.title
                            (if bool then
                                titleTrue

                             else
                                titleFalse
                            )
                        , Html.Events.onClick <| msg entry
                        ]
                        (if bool then
                            [ Html.i [ Html.Attributes.class iconTrue ] []
                            , Html.text labelTrue
                            ]

                         else
                            [ Html.i [ Html.Attributes.class iconFalse ] []
                            , Html.text labelFalse
                            ]
                        )

                textHtml : String -> Html.Html LoggedInMsg
                textHtml string =
                    case Html.Parser.run string of
                        Ok nodes ->
                            Html.div [] <|
                                Html.Parser.Util.toVirtualDom nodes

                        Err _ ->
                            Html.text ""
            in
            Html.section [ Html.Attributes.class "feed-content" ]
                [ Html.h3 []
                    [ Html.a [ Html.Attributes.href entry.link ] [ Html.text entry.title ] ]
                , if entry.image /= "" then
                    Html.div [ Html.Attributes.class "card-image" ]
                        [ Html.img [ Html.Attributes.class "img-responsive text-center", Html.Attributes.src entry.image ] [] ]

                  else
                    Html.text ""
                , textHtml entry.summary
                , textHtml entry.content
                , Html.hr [] []
                , Html.footer []
                    [ button
                        entry.flagged
                        ( "Remove from your ReRSS feed", " Unshare", "fa fa-plus-square" )
                        ( "Add to your ReRSS feed", " Share", "far fa-plus-square" )
                        Flag
                    , Html.text " "
                    , button
                        entry.bookmark
                        ( "Remove from your bookmarks", " Forget", "fa fa-bookmark" )
                        ( "Bookmark: Read it later", " Bookmark", "far fa-bookmark" )
                        Bookmark
                    , Html.text " "
                    , button
                        entry.seen
                        ( "Mark as unseen", " Mark as unseen", "far fa-envelope-open" )
                        ( "Mark as seen", " Mark as seen", "far fa-envelope" )
                        MarkSeen
                    , Html.text " "
                    , Html.a [ Html.Attributes.class "button", Html.Attributes.href entry.link, Html.Attributes.title "Open" ]
                        [ Html.i [ Html.Attributes.class "fa fa-link" ]
                            []
                        ]
                    ]
                , Html.br [] []
                ]



---- DECODERS ----


entriesDecoder : Decode.Decoder (List Entry)
entriesDecoder =
    Decode.field "entries" (Decode.list entryDecoder)


entryDecoder : Decode.Decoder Entry
entryDecoder =
    Decode.succeed Entry
        |> Pipeline.required "id" Decode.int
        |> Pipeline.required "title" Decode.string
        |> Pipeline.required "summary" Decode.string
        |> Pipeline.optional "content" Decode.string ""
        |> Pipeline.required "flagged" Decode.bool
        |> Pipeline.required "seen" Decode.bool
        |> Pipeline.required "bookmark" Decode.bool
        |> Pipeline.optional "image" Decode.string ""
        |> Pipeline.required "link" Decode.string
        |> Pipeline.required "updated" posixDecoder
        |> Pipeline.required "sources" (Decode.list feedDecoder)


feedsDecoder : Decode.Decoder (List Feed)
feedsDecoder =
    Decode.field "feeds" (Decode.list feedDecoder)


feedDecoder : Decode.Decoder Feed
feedDecoder =
    Decode.succeed Feed
        |> Pipeline.required "title" Decode.string
        |> Pipeline.required "subtitle" Decode.string
        |> Pipeline.required "link" Decode.string
        |> Pipeline.required "active" Decode.bool
        |> Pipeline.required "failed" (Decode.maybe posixDecoder)


posixDecoder : Decode.Decoder Time.Posix
posixDecoder =
    Decode.int
        |> Decode.map (\seconds -> seconds * 1000 |> Time.millisToPosix)


urlFromServer : Server -> String
urlFromServer server =
    let
        (Server serverURL) =
            server
    in
    serverURL


riskyGetRequest : String -> Decode.Decoder a -> Http.Request a
riskyGetRequest url decoder =
    Http.request
        { method = "GET"
        , headers = []
        , url = url
        , body = Http.emptyBody
        , expect = Http.expectJson decoder
        , timeout = Nothing
        , withCredentials = True
        }


riskyPostRequest : String -> Http.Body -> Decode.Decoder a -> Http.Request a
riskyPostRequest url body decoder =
    Http.request
        { method = "POST"
        , headers = []
        , url = url
        , body = body
        , expect = Http.expectJson decoder
        , timeout = Nothing
        , withCredentials = True
        }


getEntries : Server -> Cmd LoggedInMsg
getEntries server =
    riskyGetRequest (urlFromServer server ++ "/entry") entriesDecoder
        |> Http.send NewEntries


getFeeds : Server -> Cmd LoggedInMsg
getFeeds server =
    riskyGetRequest (urlFromServer server ++ "/feed") feedsDecoder
        |> Http.send NewFeeds


askToken : { a | server : String, username : String, email : String } -> Cmd LoggedOutMsg
askToken { server, username, email } =
    let
        jsonBody =
            [ ( "username", Encode.string username )
            , ( "email", Encode.string email )
            ]
                |> Encode.object
                |> Http.jsonBody
    in
    Http.request
        { method = "POST"
        , headers = []
        , url = server ++ "/token"
        , body = jsonBody
        , expect = Http.expectStringResponse (\_ -> Ok ())
        , timeout = Nothing
        , withCredentials = False
        }
        |> Http.send TokenAsked



---- UTILS ----


listReplace : a -> a -> List a -> List a
listReplace original updated list =
    list
        |> List.map
            (\element ->
                if element == original then
                    updated

                else
                    element
            )


listRemoveAtIndex : Int -> List a -> List a
listRemoveAtIndex index list =
    let
        before =
            List.take index list

        after =
            List.drop (index + 1) list
    in
    before ++ after


displayTime : Time.Posix -> Time.Zone -> String
displayTime time zone =
    let
        year =
            String.fromInt (Time.toYear zone time)

        month =
            stringFromMonth (Time.toMonth zone time)
                |> String.pad 2 '0'

        day =
            String.fromInt (Time.toDay zone time)
                |> String.pad 2 '0'

        hour =
            String.fromInt (Time.toHour zone time)
                |> String.pad 2 '0'

        minute =
            String.fromInt (Time.toMinute zone time)
                |> String.pad 2 '0'

        second =
            String.fromInt (Time.toSecond zone time)
                |> String.pad 2 '0'
    in
    year ++ "-" ++ month ++ "-" ++ day ++ " " ++ hour ++ ":" ++ minute ++ ":" ++ second


stringFromMonth : Time.Month -> String
stringFromMonth month =
    case month of
        Time.Jan ->
            "01"

        Time.Feb ->
            "02"

        Time.Mar ->
            "03"

        Time.Apr ->
            "04"

        Time.May ->
            "05"

        Time.Jun ->
            "06"

        Time.Jul ->
            "07"

        Time.Aug ->
            "08"

        Time.Sep ->
            "09"

        Time.Oct ->
            "10"

        Time.Nov ->
            "11"

        Time.Dec ->
            "12"


errorToString : Http.Error -> String
errorToString error =
    case error of
        Http.BadUrl str ->
            "bad url: " ++ str

        Http.Timeout ->
            "timeout"

        Http.NetworkError ->
            "network error"

        Http.BadStatus response ->
            "bad status: " ++ String.fromInt response.status.code ++ " " ++ response.status.message

        Http.BadPayload str response ->
            "bad body: " ++ str ++ " --- response : " ++ response.body



---- PROGRAM ----


main : Program Flags Model Msg
main =
    Browser.application
        { view = view
        , init = init
        , onUrlChange = UrlChanged
        , onUrlRequest = LinkClicked
        , update = update
        , subscriptions = subscriptions
        }



---- SUBSCRIPTIONS ----


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ syncProgress UpdateProgress
        , syncDone ProgressDone
        ]
        |> Sub.map LoggedInMsg



---- PORTS ----


port sync : String -> Cmd msg


port syncProgress : (Float -> msg) -> Sub msg


port syncDone : (Bool -> msg) -> Sub msg


port saveSession : String -> Cmd msg


port removeSession : () -> Cmd msg
